// Las acciones nos permiten hacer peticiones HTTP
// export async function fetchProducts ({state, commit, dispatch, rootState}) {
export async function fetchProducts ({commit}) {
    const data = await fetch('/fixtures/products.json');
    const products = await data.json();
    commit('products/setProducts', products, {root: true}) // Al agregarle root: true, hace la actualizacion desde el root del store
}