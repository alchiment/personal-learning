// Archivo para hacer el export y hacerse en la tienda
import state from './state';
import * as mutations from './mutations';
import * as getters from './getters'; // Si el state contiene los datos y las mutations la alteran
// Los getters filtran u obtener esos datos.

const namespaced = true;

export default {
    namespaced,
    state,
    mutations,
    getters,
}