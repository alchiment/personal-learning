import {find, filter} from 'lodash';

export function addProduct(state, product) {
    const productInCart = find(state.cart, {id: product.id});
    if (!productInCart) {
        const copy = Object.assign({}, product);
        // Se añade por primera vez
        copy.quantity  = 1;
        state.cart.push(copy);
    } else {
        productInCart.quantity += 1;
    }
}

export function removeProductFromCart(state, product) {
    // id pertenece a product
    state.cart = filter(state.cart, ({id}) => id !== product.id);
}