export function totalCost (state) {
    return state.cart.reduce((sum, product) => {
        return (parseFloat(product.price) * product.quantity) + sum;
    }, 0); // Inicia con 0
}