import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    reditect: "/todos",
  },
  {
    path: "/todos",
    name: "Todos",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Todos.vue"),
  },
  {
    path: "/todos/create",
    name: "todos-create",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/TodoCreate.vue"),
  },
  {
    path: "/todos/:id/update",
    name: "todos-update",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/TodoUpdate.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
