import Vue from "vue";

export async function fetchTodos({commit}) {
  try {
    const { data } = await Vue.axios({
      // Method by default: GET
      url: "/todos",
    });
    commit("setTodos", data);
  } catch( e ) {
    commit("todosError", e.message);
  } finally {
    console.log("end request todos");
  }
}

// export async function addTodo({commit, dispatch}, todo) {
export async function addTodo({commit}, todo) {
  // El dispatch se usa para ejecutar otra accion
  try {
    await Vue.axios({
      method: "POST",
      url: "/todos",
      data: {
        id: Date.now(),
        text: todo.text,
        done: false
      }
    });
    // dispatch("fetchTodos");
  } catch( e ) {
    commit("todosError", e.message);
  } finally {
    console.log("end request add todos");
  }
}

export async function updateTodo({commit}, todo) {
  try {
    await Vue.axios({
      method: "PUT",
      url: `/todos/${todo.id}`,
      data: {
        id: todo.id,
        text: todo.text,
        done: todo.done
      }
    });
  } catch( e ) {
    commit("todosError", e.message);
  } finally {
    console.log("end request update todos");
  }
}

export async function updateTodoStatus ({commit, dispatch}, todo) {
  try {
    await Vue.axios({
      method: 'PUT',
      url: `/todos/${todo.id}`,
      data: {
        id: todo.id,
        text: todo.text,
        done: ! todo.done
      }
    })
    dispatch('fetchTodos')
  } catch (e) {
    commit('todosError', e.message)
  } finally {
    console.log('La petición para actualizar el estado de todo ha finalizado')
  }
}

export async function removeTodo({commit, dispatch}, id) {
  try {
    await Vue.axios({
      method: "DELETE",
      url: `/todos/${id}`
    });
    dispatch('fetchTodos')
  } catch( e ) {
    commit("todosError", e.message);
  } finally {
    console.log("end request update todos");
  }
}