// Archivo para hacer el export y hacerse en la tienda
import state from './state';
import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters'; // Si el state contiene los datos y las mutations la alteran
// Los getters filtran u obtener esos datos.

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}