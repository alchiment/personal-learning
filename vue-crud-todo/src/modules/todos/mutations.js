export function setTodos(state, todos) {
  state.todos = todos;
}

export function setTodo(state, todo) {
  state.selectedTodo = todo;
}

export function updateTodoStatus(state, payload) {
  const todoSelected = state.todo.find(todo => todo.id === payload.id);
  if (todoSelected) {
    todoSelected.done = !todoSelected .done;
  }
}

export function todosError(state, payload) {
  state.error = true;
  state.errorMessage = payload;
  state.todos = [];
}