import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: { // Son los datos de la aplicacion, de Vuex
    counter: 0,
    appName: 'Counter App'
  },
  mutations: { // Sirven para alterar el estado de la app
    increment (state, quantity) {
      state.counter += quantity;
    },
    decrement (state) {
      state.counter -= 1;
    }
  },
  actions: {
  },
  modules: {
  }
})
