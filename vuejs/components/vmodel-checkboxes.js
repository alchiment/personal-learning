Vue.component('vmodel-checkboxes', {
    data () {
        return {
            frameworks: []
        }
    },
    template: `
        <div>
            <h2>vmodel con Arrays</h2>
            <input type="checkbox" id="vuejs2" v-model="frameworks" value="vuejs2">
            <label for="vuejs2">Vuejs 2</label>
            <input type="checkbox" id="reactjs" v-model="frameworks" value="reactjs">
            <label for="reactjs">Reactjs</label>
            <input type="checkbox" id="angular" v-model="frameworks" value="angular">
            <label for="angular">Angular</label>
            <p>El framework seleccionados: {{frameworks}}</p>
        </div>
    `
})