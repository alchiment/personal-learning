/**
 * Las directivas son atributos de la etiqueta como el v-for
 * Las directivas se usan con v-DIRECTIVE, ejemplo con focus:  v-focus
 */

Vue.directive('focus', {
    inserted(el) { // Se ejecuta cuando el elemento es insertado
        el.focus();
    }
});