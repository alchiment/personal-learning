/**
 * Las directivas son atributos de la etiqueta como el v-for
 */
Vue.directive('change-styles', (el, binding) => { // binding son los datos que se le pasan a la directiva
    el.style.background = binding.value.backgroundColor;
    el.style.color = binding.value.color;
});