Vue.component('load-dynamic-components', {
    data () {
        return {
            components: ['comp_1', 'comp_2', 'comp_3'],
            currentComponent: 'comp_1'
        }
    },
    methods: {
        changeComponent (cmp) {
            this.currentComponent = cmp;
        }
    },
    template: `
        <div>
            <h2>Componentes dinamicos</h2>
            <button v-for="cmp in components" :key="cmp" @click="changeComponent(cmp)">
                Seleccionar {{cmp}}
            </button>
            <!-- Carga de componentes dinamicos -->
            <!--<component v-bind:is="currentComponent"></component> es lo mismo que lo de abajo -->
            <component :is="currentComponent"></component>
        </div> 
    `
})