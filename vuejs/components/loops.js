Vue.component('loops', {
    data () {
        return {
            frameworks: [
                {id: 1, name: 'Vuejs 2'},
                {id: 2, name: 'ReactJS'},
                {id: 3, name: 'EmberJS'},
                {id: 4, name: 'Adonisjs'},
                {id: 5, name: 'Angular'},
                {id: 6, name: 'Laravel'},
            ]
        }
    },
    template: `
        <div>
            <h2>Bucles con v-for</h2>
            <ul v-if="frameworks.length > 0">
                <!-- al poner los :key reconoce el atributo como una variable -->
                <!-- Si se pone key reconoce el atributo como un string -->
                <li v-for="(framework, index) in frameworks" :key="framework.id">
                    ({{index}}) {{framework.name}}
                </li>
            </ul>
        </div> 
    `
})