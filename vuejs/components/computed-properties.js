Vue.component('computed-properties', {
    data () {
        return {
            name: 'Richard',
            surname: 'Rivas'
        }
    },
    computed: {
        // Permite concatenar variables
        // Gestionarlas o manipularlas
        fullName () {
            return `${this.name} ${this.surname}`
        }
    },
    template: `
        <div>
            <h1>Computed Propierties</h1>
            <p>
                {{name}} {{surname}}
            </p>
            <p>
                {{fullName}}
            </p>
        </div>
    `
})