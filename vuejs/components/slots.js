/**
 * Se utilizan para definir o sobreescribir layouts o secciones de código
 */
Vue.component('slots', {
    template: `
        <div>
            <h2>Slots ejemplo de layout</h2>
            <div>
                <header>
                    <slot name="header"></slot> <!-- Componente interno de Vue -->
                </header>
                <main>
                    <slot></slot>
                </main>
                <footer>
                    <slot name="footer"></slot>
                </footer>
            </div>
        </div> 
    `
})