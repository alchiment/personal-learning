/**
 * Para que un plugin funcione debe exponer el método install.
 */

const AboutMe = {
    // La variable Vue la envia Vue al momento de instanciarlo
    // La variable options son los parametros a enviar
    install: (Vue, options) => {
        const {job} = options;
        // Acceder al prototipo de Vue
        Vue.prototype.$me = (name, age) => {
            return `Mi nombre es ${name} tengo ${age} y trabajo de ${job}`;
        }
    }
}

// La forma de usar el plugin
// Al momento de usar .use de Vue, lo permite utilizar en cualquier componente
Vue.use(AboutMe, {
    job: 'Desarrollador'
});