Vue.component('computed-properties-get-set', {
    data () {
        return {
            amount: 0
        }
    },
    computed: {
        // Permite concatenar variables
        // Gestionarlas o manipularlas
        amountFormatted: {
            get () {
                return `${this.amount}`;
            },
            set (newValue){
                this.amount = newValue;
            },
        }
    },
    template: `
        <div>
            <h1>Computed Propierties get y set</h1>
            <input type="text" v-model="amount">
            <p>
                {{amountFormatted | currency_filter('$')}}
            </p>
        </div>
    `
})