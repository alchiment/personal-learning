Vue.component('child-data', {
    data() {
        return {
            cmpName: 'Child data CMP'
        }
    },
    template: `
        <div>
            <h2>Acceso a datos del CMP hijo desde el CMP padre</h2>
        </div>
    `
})