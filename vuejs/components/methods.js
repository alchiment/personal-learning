Vue.component('methods', {
    data () {
        return {
            name: 'Richard',
            surname: 'Rivas'
        }
    },
    computed: {
        // Permite concatenar variables
        // Gestionarlas o manipularlas
        fullName () {
            return `${this.name} ${this.surname}`
        }
    },
    // Van a estar disponible en el componente y podra acceder a todos los datos.
    methods: {
        hello() {
            alert(this.fullName)
        }
    },
    template: `
        <div>
            <h1>Ejecutar metodos con Vuejs</h1>
            <p @click="hello">
                Pulsa aqui para ejecutar el metodo hello
            </p>    
        </div>
    `
})