Vue.component('parent-data', {
    template: `
        <div>
            <h2>Acceso a datos del CMP padre desde el CMP hijo</h2>
            <!-- para acceder a multiples componentes padres, solo se usaria $parent.$parent... --> 
            <p>{{$parent.appName}}</p>
        </div>
    `
})