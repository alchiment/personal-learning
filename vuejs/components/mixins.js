/**
 * Mixin: una forma de heredar datos para reutilizar datos
 */
let myMixin = {
    mounted () {
        console.log('mixin init');
        console.log(this.mixinData);
        this.test();
    },
    data () {
        return {
            mixinData: 'Mixin Data'
        }
    },
    methods: {
        test() {
            console.log('test from mixin');
        }
    }
}

Vue.component('mixins', {
    mixins: [myMixin],
    mounted() { // -> Tambien puede usar el mounted
        console.log('Mounted from component with mixins');
    },
    data () {
        return {
            mixinData: 'Mixin Data desde el componente' // Sobre escribe
        }
    },
    template: `
        <div>
            <h2>Uso de Mixins</h2>
            <p>{{mixinData}}</p>
        </div> 
    `
});