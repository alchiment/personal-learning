/**
 * Tener cuidado ya que utiliza mas recursos de la maquina.
 * Casi todo lo que se pueda hacer con Watchers, se puede hacer con computed.
 */

Vue.component('watchers', {
    data () {
        return {
            user: null,
            oldUser: null
        }
    },
    // mounted() {
    //     this.randomUser();
    // },
    methods: {
        async randomUser() {
            try {
                const data = await fetch('https://randomuser.me/api/');
                const json = await data.json();
                const user = json.results[0];
                this.user = `${user.name.title} ${user.name.first} ${user.name.last}`;
                console.log('json', user);
            } catch (e) {
                console.error(e);
            }
        }
    },
    watch: {
        user (newVal, oldVal) {
            this.user = newVal;
            this.oldUser = oldVal;
        }
    },
    template: `
        <div>
            <h2>Watchers con Vuejs</h2>
            <button @click="randomUser()">Obtener un usuario aleatorio</button>
            <p>Nuevo usuario: {{user}}</p>
            <p>Anterior usuario: {{oldUser}}</p>
        </div>
    `
})