Vue.component('child-methods', {
    data() {
        return {
            cmpName: 'Child method CMP'
        }
    },
    methods: {
        showCmpName() {
            console.log('child:', this.cmpName);
        }
    },
    template: `
        <div>
            <h2>Acceso a métodos del CMP hijo desde el CMP padre</h2>
        </div>
    `
})