Vue.component('emit', {
    data () {
        return {
            carBrand: 'Toyota'
        }
    },
    template: `
        <div>
            <h2>Emitir eventos con Vuejs 2</h2>
            <!-- $emit('EVENTO A EMITIR', DATO A PASAR) -->
            <p @click="$emit('show_car_brand', carBrand)"> <!-- No se puede usar camelcase -->
                Pulsar aqui para emitir un evento a la instancia root de Vuejs
            </p>
        </div>
    `
})