/**
 * Los props son variables que se pueden introducir al componente al momento de inicializarlo.
 * Se utilizan como atributos del componente
 */
Vue.component('props', {
    // props: ['name'],
    props: {
        name: {
            type: String,
            required: true
        },
        lastname: {
            type: String,
            required: true
        },
        age: {
            type: Number,
            required: true,
            validator: value => {
                if (value < 18) {
                    console.warn('No eres mayor de edad');
                    return false;
                }
                return true;
            }
        }
    },
    template: `
        <div>
            <h2>Props con Vuejs 2</h2>
            <p>{{name}} {{lastname}}</p>
        </div>
    `
});