Vue.component('message', {
    data () {
        return {
            message: 'Hola mundo message'
        }
    },
    template: `
        <div>
            <h1>{{message}}</h1>
        </div>
    `

})