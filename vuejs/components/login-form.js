Vue.component('login-form', {
    data () {
        return {
            logged: false,
            user: {
                email: '',
                password: ''
            }
        }
    },
    methods: {
        login () {
            this.logged = this.user.email === 'test@m.com' && this.user.password === "1234";
        }
    },
    template: `
        <div>
            <h2>Formulario login</h2>
            <p v-show="logged" style="background: grey; color: white;">
                Has iniciado sesión con los datos: {{user}}
            </p>
            <form @submit.prevent="login" v-show="!logged">
                <input type="email" v-model="user.email" name="email" id="email">
                <input type="password" v-model="user.password" name="password" id="password">
                <button type="submit">Iniciar sesión</button>
            </form>
        </div> 
    `
})